<?php

declare(strict_types=1);

// Delegate static file requests back to the PHP built-in webserver
if (PHP_SAPI === 'cli-server' && $_SERVER['SCRIPT_FILENAME'] !== __FILE__) {
    return false;
}

chdir(dirname(__DIR__));
require 'src/Infrastructure/Service/WkHtmlToPdfService.php';

/**
 * Self-called anonymous function that creates its own scope and keeps the global namespace clean.
 */
(function () {
    $input = file_get_contents('php://input');

    try {
        $wkHtmlToPdfService = new WkHtmlToPdfService();

        if (isset($_GET['help'])) {
            $result = $wkHtmlToPdfService->help();

            header('Content-Type: text/plain');
            echo $result;

            return;
        }

        $result = $wkHtmlToPdfService->output($input, $_GET);

        header('Content-Type: application/pdf; charset=utf-8');
        echo $result;
    } catch (Throwable $t) {
        echo $t;
    }
})();
